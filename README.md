# Installing the modules

Cd into the project's folder.
Run `npm install` to install the modules.

# Send data over MQTT

Run `node app.js mqttDeviceDemo --temp=30` to send a message with a temperature value of 30.

Everytime you run the previous command, you'll send a message in the format \
`{ temp: number, timestamp: number }`, where temp is set by the value you pass \
from the command `--temp=X`.
