"use strict";
const fs = require("fs");
const jwt = require("jsonwebtoken");
const mqtt = require("mqtt");

const PROJECT_ID = "mpower-mqtt";
const REGISTRY_ID = "test-registry";
const DEVICE_ID = "test-device";
const REGION = "europe-west1";
const ALGORITHM = "RS256";
const PRIVATE_KEY = "rsa_private.pem";

const mqttDeviceDemo = (temp) => {
  console.log("Starting connection...");
  const jwt = createJwt(PROJECT_ID, PRIVATE_KEY, ALGORITHM);

  const mqttClientId = `projects/${PROJECT_ID}/locations/${REGION}/registries/${REGISTRY_ID}/devices/${DEVICE_ID}`;
  const mqttTopic = `/devices/${DEVICE_ID}/events`;
  const connectionArgs = {
    host: "mqtt.googleapis.com",
    port: 8883,
    clientId: mqttClientId,
    username: "unused",
    password: jwt,
    protocol: "mqtts",
    secureProtocol: "TLSv1_2_method",
  };

  const client = mqtt.connect(connectionArgs);
  client.on("connect", (success) => {
    !success
      ? console.log("Client not connected...")
      : publishAsync(mqttTopic, client, temp);
  });

  client.on("close", () => {
    console.log("close");
  });

  client.on("error", (err) => {
    console.log("error", err);
  });

  client.on("message", (topic, message) => {
    console.log("message received:");
  });
};

const createJwt = (projectId, privateKeyFile, algorithm) => {
  const token = {
    iat: parseInt(Date.now() / 1000),
    exp: parseInt(Date.now() / 1000) + 20 * 60, // 20 minutes
    aud: projectId,
  };
  const privateKey = fs.readFileSync(privateKeyFile);
  return jwt.sign(token, privateKey, { algorithm: algorithm });
};

const publishAsync = (mqttTopic, client, temp) => {
  const payload = `{ temp: ${temp}, timestamp: ${Number(new Date())} }`;
  console.log("Publishing message:", payload);

  client.publish(mqttTopic, payload, { qos: 1 }, (err) => {
    !err ? console.log("Published") : console.log("There was an error");
    console.log("Closing connection to MQTT. Goodbye!");
    client.end();
  });
};

console.log("Google Cloud IoT Core MQTT example.");
const { argv } = require(`yargs`)
  .options({
    temp: {
      default: 100,
      description: "Temperature of the device",
      requiresArg: true,
      type: "number",
    },
  })
  .command(
    `mqttDeviceDemo`,
    `Connects a device, sends data, and receives data`,
    {},
    (opts) => {
      mqttDeviceDemo(opts.temp);
    }
  )
  .wrap(120)
  .recommendCommands()
  .epilogue(`For more information, see https://cloud.google.com/iot-core/docs`)
  .help()
  .strict();
